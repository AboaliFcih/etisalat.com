/**
 * CopyTasks
 * Provide gulp tasks that manage to copy resources from a source folder to a destination folder,
 * while doing some transformation in the middle if necessary.
 * Every task is specialized for a specific resource.
 * All of the source paths are defined in the gulp configuration in gulp.config.js file.
 *
 */
function CopyTasks(gulp, $, config) {
  'use strict';

  var util = require('./util')($);

  /**
   * private funciton that return the current date in the format YYYYMMDD
   * used to versioning the source files
   */
  var getVersionDate = function () {
    var now = new Date();
    var date = [now.getFullYear(), now.getMonth() + 1, now.getDate()];
    if (date[1] < 10) {
      date[1] = '0' + date[1];
    }
    if (date[2] < 10) {
      date[2] = '0' + date[2];
    }

    var vdate = date[0].toString() + date[1].toString() + date[2].toString();

    return vdate;
  };


  /**
   * Task: copyJs
   * Includes several sub-tasks that take the js files, transform them, and then copy them to the 'dist/assets/js' folder.
   * For details, see every sub-tasks comments.
   */
  function copyJs() {
    var vdate = getVersionDate();

    /**
     * steps:
     * 1) source: config.paths.javascript.modules
     * 2) add sourcemaps
     * 3) minify with uglify
     * 4) copy to 'dist/assets/js/modules'
     *
     */
    gulp.src(config.paths.javascript.modules)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.replace('###version###', vdate))
      .pipe($.sourcemaps.init())
      .pipe($.rename({ suffix: '.min' }))
      .pipe($.uglify())
      .pipe($.sourcemaps.write('./' /* , { sourceRoot: 'assets/js/modules/' }*/))
      .pipe(gulp.dest('dist/assets/js/modules'));

      /**
       * steps:
       * 1) source: config.paths.javascript.modules
       * 4) copy to 'dist/assets/js/modules'
       *
       */
      gulp.src(config.paths.javascript.modules)
        .pipe($.plumber(util.plumberErrorHandler))
        .pipe($.replace('###version###', vdate))
        .pipe($.sourcemaps.write('./' /* , { sourceRoot: 'assets/js/modules/' }*/))
        .pipe(gulp.dest('dist/assets/js/modules'));



    /**
     * steps:
     * 1) source: config.paths.javascript.js
     * 2) copy to 'dist/assets/js'
     *
     */
    gulp.src(config.paths.javascript.js)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.replace('###version###', vdate))
      .pipe(gulp.dest('dist/assets/js'));


    /**
     * steps:
     * 1) source: config.paths.javascript.js
     * 2) add sourcemaps
     * 3) minify with uglify
     * 4) copy to 'dist/assets/js'
     *
     */
    gulp.src(config.paths.javascript.js)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.replace('###version###', vdate))
      .pipe($.sourcemaps.init())
      .pipe($.rename({ suffix: '.min' }))
      .pipe($.uglify())
      .pipe($.sourcemaps.write('./' /* ,{ sourceRoot: 'assets/js/' }*/))
      .pipe(gulp.dest('dist/assets/js'));


    /**
     * steps:
     * 1) source: config.paths.javascript.datepicker
     * 2) add sourcemaps
     * 3) concatenate all the source files in a single file 'pickadate.min.js'
     * 4) minify with uglify
     * 5) copy to 'dist/assets/js'
     *
     */
    gulp.src(config.paths.javascript.datepicker)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.sourcemaps.init())
      .pipe($.concat('pickadate.min.js'))
      .pipe($.uglify())
      .pipe($.sourcemaps.write('./' /* ,{ sourceRoot: 'assets/js/' }*/))
      .pipe(gulp.dest('dist/assets/js'));


    /**
     * steps:
     * 1) source: config.paths.javascript.timepicker
     * 2) add sourcemaps
     * 3) concatenate all the source files in a single file 'pickatime.min.js'
     * 4) minify with uglify
     * 5) copy to 'dist/assets/js'
     *
     */
    return gulp.src(config.paths.javascript.timepicker)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.sourcemaps.init())
      .pipe($.concat('pickatime.min.js'))
      .pipe($.uglify())
      .pipe($.sourcemaps.write('./' /* ,{ sourceRoot: 'assets/js/' }*/))
      .pipe(gulp.dest('dist/assets/js'));


  }


  /**
   * Task: copyCss
   * Copy the css files in source folder to the destination.
   * Usually used for 3rdy party stylesheets
   * steps:
   * 1) source: config.paths.css
   * 2) copy to 'dist/assets/css'
   *
   */
  function copyCss() {
    return gulp.src(config.paths.css)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist/assets/css'));
  }


  /**
   * Task: copyFonts
   * Copy the web fonts from source to the destination.
   * steps:
   * 1) source: config.paths.fonts
   * 2) copy to 'dist/assets/fonts'
   *
   */
  function copyFonts() {
    return gulp.src(config.paths.fonts)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('dist/assets/fonts'));
  }


  /**
   * Task: copyOthers
   * Copy some specific file like robots.txt and web.config from source to destination.
   * steps:
   * 1) source: config.paths.others
   * 2) copy to 'dist'
   *
   */
  function copyOthers() {
    return gulp.src(config.paths.others)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist'));
  }

  /**
   * Task: copyKML
   * Copy kml files used for network coverage.
   * steps:
   * 1) source: config.paths.others
   * 2) copy to 'dist/assets/kml/'
   *
   */
  function copyKML() {
    return gulp.src(config.paths.kml)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist/assets/kml'));
  }


  /**
   * Task: copyFavicon
   * Copy the favicon file from source to destination.
   * steps:
   * 1) source: config.paths.favicon
   * 2) copy to 'dist/assets'
   *
   */
  function copyFavicon() {
    return gulp.src(config.paths.favicon)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist/assets'));
  }


  /**
   * Task: copyMockData
   * Copy the mock json data files from source to destination.
   * steps:
   * 1) source: config.paths.mockdata
   * 2) copy to 'dist/assets/mock-data'
   *
   */
  function copyMockData() {
    return gulp.src(config.paths.mockdata)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist/assets/mock-data'));
  }


  /**
   * Task: copyImages
   * Copy the images files from source to destination.
   * steps:
   * 1) source: config.paths.images
   * 2) copy to 'dist/assets/img'
   *
   */
  function copyImages() {
    return gulp.src(config.paths.images)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist/assets/img'));
  }


  /**
   * Task: optimizeImages
   * Take the source images, optimize them with ImageMin, and then copy them to destination.
   * steps:
   * 1) source: config.paths.images
   * 2) optimize images with ImageMin package
   * 2) copy to 'dist/assets/img'
   *
   */
  function optimizeImages() {
    var imageminMozjpeg = require('imagemin-mozjpeg');

    return gulp.src(config.paths.images)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.imagemin({
        optimizationLevel: 7,
        progressive: true,
        interlaced: true,
        multipass: true,
        use: [imageminMozjpeg()]
      }))
      .pipe(gulp.dest('./dist/assets/img'))
      .pipe($.size({ title: 'images' }));
  }


  return {
    copyJs: copyJs,
    copyCss: copyCss,
    copyFonts: copyFonts,
    copyOthers: copyOthers,
    copyFavicon: copyFavicon,
    copyImages: copyImages,
    images: optimizeImages,
    copyMockData: copyMockData,
    copyKML:copyKML
  };
}

module.exports = CopyTasks;
