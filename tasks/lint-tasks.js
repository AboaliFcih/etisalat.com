/**
 * This class contains all the linting methods for a static analysis
 * of JavaScript , CSS, and HTML code.
 * @class LintTasks
 * @param {Object} gulp The Gulp module
 * @param {Object} $ The gulp-load-plugins module
 * @param {Object} config Gulp configuration object
 * @returns {{eslint: LintTasks.eslint}}
 * @constructor
 */
function LintTasks (gulp, $, config) {
  'use strict';

  var util = require('./util')($);


  /**
   * Run ESLint for all the JavaScript files in the 'src/js' folder.
   * @method eslint
   * @memberof LintTasks
   * @returns {*}
   */
  function eslint () {
    return gulp.src(['./src/assets/js/**/*.js'])
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.eslint(config.paths.settings.eslint))
      .pipe($.eslint.format())
      .pipe($.eslint.results(function (results) {
        // Called once for all ESLint results.
        console.log('Total Results: ' + results.length);
        console.log('Total Warnings: ' + results.warningCount);
        console.log('Total Errors: ' + results.errorCount);
      }))
      .pipe($.eslint.failAfterError());
  }


  return {
    eslint: eslint
  };

}

module.exports = LintTasks;
