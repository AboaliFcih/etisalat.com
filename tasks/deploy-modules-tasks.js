/**
 * This class contains the methods to build the Module Library documentation
 * @class DeployModulesTasks
 * @param {Object} gulp The Gulp module
 * @param {Object} $ The gulp-load-plugins module
 * @param {Object} config Gulp configuration object
 * @returns {{deployModules: deployModules}}
 * @constructor
 */
function DeployModulesTasks (gulp, $, config) {
  'use strict';

  var util = require('./util')($);
  var merge = require('merge-stream');
  var _ = require('lodash');
  var Handlebars = require('handlebars');
  var globby = require('globby');
  var fs = require('fs');
  var mkdirp = require('mkdirp');
  var path = require('path');
  var matter = require('gray-matter');
  var beautifyHtml = require('js-beautify').html;
  var dictionary = require('../src/data/dictionary.json');


  /**
   * Get the name of a file (minus extension) from a path
   * @param  {String} filePath
   * @example
   * './src/materials/structures/foo.html' -> 'foo'
   * './src/materials/structures/02-bar.html' -> 'bar'
   * @return {String}
   */
  var getName = function (filePath, preserveNumbers) {
    var name = path.basename(filePath, path.extname(filePath));
    return (preserveNumbers) ? name : name.replace(/^[0-9|\.\-]+/, '');
  };


  /**
   * Attempt to read front matter, handle errors
   * @param  {String} file Path to file
   * @return {Object}
   */
  var getMatter = function (file) {
    return matter.read(file, {
      parser: require('js-yaml').safeLoad
    });
  };


  /**
   * get the module name without suffix to be used as folder name
   */
  function getFolderName (fileName) {
    var folderName = '';
    var parts = fileName.split('-');
    for (var index = 0; index < parts.length; index++) {
      if (index === 0) {
        folderName = folderName + parts[index] + '-';
        continue;
      }

      if (!_.isNaN(_.parseInt(parts[index], 10))) {
        break;
      }

      folderName = folderName + parts[index] + '-';
    }

    if (folderName.substr(folderName.length - 1, 1) === '-') {
      folderName = folderName.substring(0, folderName.length - 1);
    }

    return folderName;
  }


  /**
   * Get the dictionary for the lang set in the page
   * @param key
   * @returns {*}
   */
  function dict (key) {
    if (!this.lang) {
      return '{nd}';
    }

    var result = dictionary[this.lang];
    var keys = key.split('.');

    for (var index = 0; index < keys.length; index++) {
      if (result) {
        result = result[keys[index]];
      }
    }

    // return result;
    return new Handlebars.SafeString(result);
  }


  /**
   * deploy the modules in the 'deploy' folder, along with the required module stylesheets and javascripts.
   */
  function deployModules () {

    // parse materials
    var options = {};
    options.dest = './dist/modules'; // './deploy/modules/'; //
    options.materials = ['src/materials/**/*'];
    options.helpers = {
      // assetHash: assetHash,
      // markdown: require('helper-markdown'),
      dict: dict
    };

    Handlebars.registerHelper('dict', options.helpers.dict);


    var files = globby.sync(options.materials, { nodir: true, nosort: true });

    // build a glob for identifying directories
    var dirsGlob = options.materials.map(function (pattern) {
      return path.dirname(pattern) + '/*/';
    });

    // get all directories
    // do a new glob; trailing slash matches only dirs
    var dirs = globby.sync(dirsGlob).map(function (dir) {
      return path.normalize(dir).split(path.sep).slice(-2, -1)[0];
    });


    /**
     * iterate over each file (material)
     */
    files.forEach(function (file) {

      // get info
      var fileMatter = getMatter(file);
      var collection = getName(path.normalize(path.dirname(file)).split(path.sep).pop(), true);
      var parent = path.normalize(path.dirname(file)).split(path.sep).slice(-2, -1)[0];
      var isSubCollection = false;
      var isSubFolder = (dirs.indexOf(parent) > -1);
      var subFolder = isSubFolder ? collection : null;
      collection = isSubFolder ? parent : collection;
      var id = getName(file, true); // get the name with numbers
      var key = getName(file, true); // get the name with numbers

      // get material front-matter, omit `notes`
      var localData = _.omit(fileMatter.data, 'notes');
      var content = fileMatter.content;
      //
      // // capture meta data for the material
      // assembly.materials[collection].items[key] = {
      //   name: fileMatter.data.title || toTitleCase(id),
      //   notes: (fileMatter.data.notes) ? md.render(fileMatter.data.notes) : '',
      //   data: localData
      // };

      // if (isSubFolder) {
      //   assembly.groupedMaterials[subFolder].items[key] = {
      //     name: fileMatter.data.title || toTitleCase(id),
      //     notes: (fileMatter.data.notes) ? md.render(fileMatter.data.notes) : '',
      //     data: localData
      //   };
      //
      //   options.debug ? util.log('groupedMaterials: adding assembly.groupedMaterials["subFolder = ' + subFolder + '"].items["' + key + '"]') : null;
      // }

      // // store material-name-spaced local data in template context
      // assembly.materialData[id.replace(/\./g, '-')] = localData;


      // replace local fields on the fly with name-spaced keys
      // this allows partials to use local front-matter data
      // only affects the compilation environment
      if (!_.isEmpty(localData)) {
        _.forEach(localData, function (val, key) {
          // {{field}} => {{material-name.field}}
          var regex = new RegExp('(\\{\\{[#\/]?)(\\s?' + key + '+?\\s?)(\\}\\})', 'g');
          content = content.replace(regex, function (match, p1, p2, p3) {
            return p1 + id.replace(/\./g, '-') + '.' + p2.replace(/\s/g, '') + p3;
          });
        });
      }

      // register the partial
      Handlebars.registerPartial(id, content);
    });


    // delete olf folder
    var del = require('del');
    del.sync(options.dest);


    // copy modules html files
    return gulp.src('src/materials/components/**/*.html')
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest(function (file) {

        var fileName = file.path.substring(file.base.length);

        var getMatter = function (file) {
          return matter.read(file, {
            parser: require('js-yaml').safeLoad
          });
        };
        var pageMatter = getMatter(file.path);
        // console.log(file.path);

        pageMatter.data.fabricator = true;
        pageMatter.data.consumer = true;
        pageMatter.data.business = false;
        pageMatter.data.lang = 'en';
        pageMatter.data.direction = 'ltr';
        var pageContent = pageMatter.content;
        var context = pageMatter.data;

        var template = Handlebars.compile(pageContent);
        /* eslint-disable indent_char camelcase */
        var content = new Buffer(beautifyHtml(template(context), {
          indent_handlebars: true,
          indent_inner_html: true,
          preserve_newlines: false,
          max_preserve_newlines: 1,
          brace_style: 'expand',
          indent_char: ' ',
          indent_size: 2
        })); 


        file.contents = content;

        // return './deploy/modules/' + getFolderName(fileName.substring(0, fileName.lastIndexOf('.')));
        return options.dest;
      }));


  }


  return {
    deployModules: deployModules
  };

}

module.exports = DeployModulesTasks;
