/**
 * This class contains all the methods necessary to build the HTML pages.
 * @class PagesTasks
 * @param {Object} gulp The Gulp module
 * @param {Object} $ The gulp-load-plugins module
 * @param {Object} config Gulp configuration object
 * @returns {{copyJs: PagesTasks.copyJs, copyCss: PagesTasks.copyCss, copyFonts: PagesTasks.copyFonts, copyOthers: PagesTasks.copyOthers, copyFavicon: PagesTasks.copyFavicon, copyImages: copyImages, images: PagesTasks.images, copyMockData: PagesTasks.copyMockData}}
 * @constructor
 */
function PagesTasks(gulp, $, config) {
  'use strict';

  var util = require('./util')($);

  var getVersionDate = function () {
    var now = new Date();
    var date = [now.getFullYear(), now.getMonth() + 1, now.getDate()];
    if (date[1] < 10) {
      date[1] = '0' + date[1];
    }
    if (date[2] < 10) {
      date[2] = '0' + date[2];
    }

    var vdate = date[0].toString() + date[1].toString() + date[2].toString();

    return vdate;
  };

  /**
   * Copy the required JavaScript files to the 'dist/assets/js' folder.
   * @method copyJs
   * @memberof PagesTasks
   * @returns {*}
   */
  function copyJs() {
    var vdate = getVersionDate();

    gulp.src(config.paths.javascript.modules)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.replace('###version###', vdate))
      .pipe($.sourcemaps.init())
      .pipe($.rename({
        suffix: '.min'
      }))
      .pipe($.uglify())
      .pipe($.sourcemaps.write('./' /* , { sourceRoot: 'assets/js/modules/' }*/))
      .pipe(gulp.dest('dist/assets/js/modules'));

    // gulp.src(config.paths.javascript.validation)
    //   .pipe($.plumber(util.plumberErrorHandler))
    //   .pipe($.sourcemaps.init())
    //   .pipe($.rename({ suffix: '.min' }))
    //   .pipe($.uglify())
    //   .pipe($.sourcemaps.write('./' /* ,{ sourceRoot: 'assets/js/' }*/))
    //   .pipe(gulp.dest('dist/assets/js'));


    /**
     * config.paths.javascript.js: source
     */
    gulp.src(config.paths.javascript.js)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.replace('###version###', vdate))
      .pipe(gulp.dest('dist/assets/js'));

    /**
     * config.paths.javascript.js: minified
     */
    gulp.src(config.paths.javascript.js)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.replace('###version###', vdate))
      .pipe($.sourcemaps.init())
      .pipe($.rename({
        suffix: '.min'
      }))
      .pipe($.uglify())
      .pipe($.sourcemaps.write('./' /* ,{ sourceRoot: 'assets/js/' }*/))
      .pipe(gulp.dest('dist/assets/js'));

    /**
     * Datepicker (pickadate)
     */
    gulp.src(config.paths.javascript.datepicker)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.sourcemaps.init())
      .pipe($.concat('pickadate.min.js'))
      .pipe($.uglify())
      .pipe($.sourcemaps.write('./' /* ,{ sourceRoot: 'assets/js/' }*/))
      .pipe(gulp.dest('dist/assets/js'));

    /**
     * Timepicker (pickatime)
     */
    return gulp.src(config.paths.javascript.timepicker)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.sourcemaps.init())
      .pipe($.concat('pickatime.min.js'))
      .pipe($.uglify())
      .pipe($.sourcemaps.write('./' /* ,{ sourceRoot: 'assets/js/' }*/))
      .pipe(gulp.dest('dist/assets/js'));


  }


  /**
   * Copy the stylesheets to the 'dist/assets/css' folder.
   * @method copyCss
   * @memberof PagesTasks
   * @returns {*}
   */
  function copyCss() {
    return gulp.src(config.paths.css)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist/assets/css'));
  }


  /**
   * Copy the web fonts to the 'dist/assets/fonts' folder.
   * @method copyFonts
   * @memberof PagesTasks
   * @returns {*}
   */
  function copyFonts() {
    return gulp.src(config.paths.fonts)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('dist/assets/fonts'));
  }


  /**
   * Copy other files to the 'dist' folder.
   * @method copyOthers
   * @memberof PagesTasks
   * @returns {*}
   */
  function copyOthers() {
    return gulp.src(config.paths.others)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist'));
  }


  /**
   * Copy favicon files to the 'dist' folder.
   * @method copyFavicon
   * @memberof PagesTasks
   * @returns {*}
   */
  function copyFavicon() {
    return gulp.src(config.paths.favicon)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist/assets'));
  }


  /**
   * Copy mock data files to the 'dist' folder.
   * @method copyMockData
   * @memberof PagesTasks
   * @returns {*}
   */
  function copyMockData() {
    return gulp.src(config.paths.mockdata)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist/assets/mock-data'));
  }


  /**
   * Copy all the images to the 'dist' folder.
   */
  function copyImages() {
    return gulp.src('./src/assets/img/**/*')
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe(gulp.dest('./dist/assets/img'));
  }


  /**
   * Process the images with ImageMin and then them to the 'dist/assets/img' folder.
   * @method images
   * @memberof PagesTasks
   * @returns {*}
   */
  function images() {
    var imageminMozjpeg = require('imagemin-mozjpeg');

    //    return gulp.src('./src/assets/img/**/*')
    return gulp.src('./src/assets/img/main-menu/*.*')
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.imagemin({
        optimizationLevel: 7,
        progressive: true,
        interlaced: true,
        multipass: true,
        use: [imageminMozjpeg()]
      }))
      .pipe(gulp.dest('./dist/assets/img/main-menu'))
      .pipe($.size({
        title: 'images'
      }));
  }


  return {
    copyJs: copyJs,
    copyCss: copyCss,
    copyFonts: copyFonts,
    copyOthers: copyOthers,
    copyFavicon: copyFavicon,
    copyImages: copyImages,
    images: images,
    copyMockData: copyMockData
  };
}

module.exports = PagesTasks;
