/**
 * @class scssTasks
 * @param {Object} gulp The Gulp module
 * @param {Object} $ The gulp-load-plugins module
 * @param {Object} config Gulp configuration object
 * @returns {{scss: scssTasks.scss}}
 * @constructor
 */

function scssTasks(gulp, $, config, browser) {
  'use strict';

  var util = require('./util')($);

  var getVersionDate = function () {
    var now = new Date();
    var date = [now.getFullYear(), now.getMonth() + 1, now.getDate()];
    if (date[1] < 10) {
      date[1] = '0' + date[1];
    }
    if (date[2] < 10) {
      date[2] = '0' + date[2];
    }

    var vdate = date[0].toString() + date[1].toString() + date[2].toString();

    return vdate;
  };


  /**
   * Compile the scss files into app.css and app.min.css,
   * applying autoprefixer, csscomb (https://github.com/csscomb/csscomb.js), sourcemaps
   * and copy them to the dist folder.
   * @method scss
   * @memberof scssTasks
   * @param callback
   */

  function scss(callback) {

    var vdate = getVersionDate();

    // compile scss into app.css
    gulp.src(config.paths.scss.app)
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.replace('###version###', vdate))
      .pipe($.sourcemaps.init())
      .pipe($.scss())
      .pipe($.autoprefixer(config.autoprefixer))
      // .pipe($.csscomb('.csscomb.json'))
      .pipe($.sourcemaps.write('./', {
        sourceRoot: '/src/assets/scss/'
      }))
      .pipe(gulp.dest('./dist/assets/css'));


    // compile scss files and minimize them into app.min.css
    gulp.src(config.paths.scss.app, {
      base: './src/assets/scss'
    })
    .pipe($.plumber(util.plumberErrorHandler))
    .pipe($.rename('app.min.css'))
    .pipe($.replace('###version###', vdate))
    .pipe($.sourcemaps.init())
    .pipe($.scss())
    .pipe($.autoprefixer(config.autoprefixer))
    // .pipe($.csscomb('.csscomb.json'))
    .pipe($.cssnano())
    .pipe($.sourcemaps.write('.', {
      sourceRoot: './src/assets/scss/'
    }))
    .pipe(gulp.dest('./dist/assets/css'))
    .pipe(browser.stream({
      match: '**/*.css'
    }));

    callback();
  }


  /**
   * scssModules
   * @param callback
   */
  function scssModules() {
    return gulp.src('./src/assets/scss/modules/*.scss')
      .pipe($.plumber(util.plumberErrorHandler))
      .pipe($.sourcemaps.init())
      .pipe($.scss())
      .pipe($.autoprefixer(config.autoprefixer))
      // .pipe($.csscomb('.csscomb.json'))
      .pipe($.sourcemaps.write('./', {
        sourceRoot: '/src/assets/scss/'
      }))
      .pipe(gulp.dest('./dist/assets/css/modules'));
  }


  return {
    scss: scss,
    scssModules: scssModules
  };

}

module.exports = scssTasks;
