/**
 * Collection of common helper functions used by some gulp tasks
 * @class Util
 * @returns {{less: LessTasks.less}}
 * @constructor
 */
function Util ($) {

  /**
   * Plumber option object that implement an error handler
   * @memberof Util
   * @type {{errorHandler: Util.plumberErrorHandler.errorHandler}}
   */
  var plumberErrorHandler = {
    errorHandler: function (error) {
      $.notify.onError({
        title: 'Gulp Task',
        message: 'Error: <%= error.message %>'
      });
      $.util.log(error.toString()); // Prints Error to Console
      this.emit('end'); // End function
    }
  };

  return {
    plumberErrorHandler: plumberErrorHandler
  };
}

module.exports = Util;
