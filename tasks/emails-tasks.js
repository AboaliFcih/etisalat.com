var rimraf   = require('rimraf');
var panini   = require('panini');
var yargs    = require('yargs');
var lazypipe = require('lazypipe');
var inky     = require('inky');
var fs       = require('fs');
var siphon   = require('siphon-media-query');
var path     = require('path');
var merge    = require('merge-stream');
// var beep     = require('beepbeep');
var colors   = require('colors');

/**
 * @class EmailsTasks
 * @param {Object} gulp The Gulp module
 * @param {Object} $ The gulp-load-plugins module
 * @param {Object} config Gulp configuration object
 * @returns {{clean: clean, zip: zip, scss: scss, images: images, inline: inline, compile: compile}}
 * @constructor
 */
function EmailsTasks (gulp, $, config, browser) {
  'use strict';

  var util = require('./util')($);

  // // Build the "dist" folder by running all of the above tasks
  // gulp.task('build',
  //   gulp.series(clean, pages, sass, images, inline));


  // Delete the "dist" folder
  // This happens every time a build starts
  function clean (done) {
    rimraf('./dist/emails', done);
  }

// Copy and compress into Zip
  function zip () {
    var dist = 'dist';
    var ext = '.html';

    function getHtmlFiles (dir) {
      return fs.readdirSync(dir)
        .filter(function (file) {
          var fileExt = path.join(dir, file);
          var isHtml = path.extname(fileExt) === ext;
          return fs.statSync(fileExt).isFile() && isHtml;
        });
    }

    var htmlFiles = getHtmlFiles(dist);

    var moveTasks = htmlFiles.map(function (file) {
      var sourcePath = path.join(dist, file);
      var fileName = path.basename(sourcePath, ext);

      var moveHTML = gulp.src(sourcePath)
        .pipe($.rename(function (path) {
          path.dirname = fileName;
          return path;
        }));

      var moveImages = gulp.src(sourcePath)
        .pipe($.htmlSrc({ selector: 'img' }))
        .pipe($.rename(function (path) {
          path.dirname = fileName + '/assets/img';
          return path;
        }));

      return merge(moveHTML, moveImages)
        .pipe($.zip(fileName + '.zip'))
        .pipe(gulp.dest('dist'));
    });

    return merge(moveTasks);
  }


  function compile () {
    return gulp.src('emails/src/pages/**/*.html')
      .pipe(panini({
        root: 'emails/src/pages',
        layouts: 'emails/src/layouts',
        partials: 'emails/src/partials',
        helpers: 'emails/src/helpers'
      }))
      .pipe(inky())
      .pipe(gulp.dest('dist/emails'));
  }

// Compile Sass into CSS
  function scss () {
    return gulp.src('emails/src/assets/scss/app.scss')
      .pipe($.sourcemaps.init())
      .pipe($.sass({
        includePaths: ['node_modules/foundation-emails/scss']
      }).on('error', $.sass.logError))
      .pipe($.sourcemaps.write())
      .pipe(gulp.dest('dist/emails/css'));
  }


  // Copy and compress images
  function images () {
    return gulp.src('emails/src/assets/img/**/*')
      .pipe($.imagemin())
      .pipe(gulp.dest('./dist/emails/assets/img'));
  }

  // Inlines CSS into HTML, adds media query CSS into the <style> tag of the email, and compresses the HTML
  function inliner (cssFile) {
    var css = fs.readFileSync(cssFile).toString();
    var mqCss = siphon(css);

    var inlinePipe = lazypipe()
      .pipe($.inlineCss, {
        applyStyleTags: false,
        removeStyleTags: false,
        removeLinkTags: false
      })
      .pipe($.replace, '<!-- <style> -->', '<style>' + mqCss + '</style>')
      // .pipe($.htmlmin, {
      //   collapseWhitespace: true,
      //   minifyCSS: true
      // })
      ;

    return inlinePipe();
  }

  // Inline CSS and minify HTML
  function inline () {
    return gulp.src('dist/emails/**/*.html')
      .pipe(inliner('dist/emails/css/app.css'))
      .pipe(gulp.dest('dist/emails'));
  }


// // Start a server with LiveReload to preview the site in
//   function server(done) {
//     browser.init({
//       server: 'dist'
//     });
//     done();
//   }


  return {
    clean: clean,
    zip: zip,
    scss: scss,
    images: images,
    inline: inline,
    compile: compile
  };

}

module.exports = EmailsTasks;
