/**
 * This class contains the methods to build the Module Library documentation
 * @class ModuleLibraryTasks
 * @param {Object} gulp The Gulp module
 * @param {Object} $ The gulp-load-plugins module
 * @param {Object} config Gulp configuration object
 * @returns {{styles: ModuleLibraryTasks.styles, scripts: ModuleLibraryTasks.scripts}}
 * @constructor
 */
function ModuleLibraryTasks(gulp, $, config) {
  'use strict';

  var util = require('./util')($);
  var merge = require('merge-stream');
  var _ = require('lodash');
  var Handlebars = require('handlebars');


  /**
   * Run fabricator to assemble all the library pages, components, etc...
   * and copy it to the dist/module-library folder
   * @method assemble
   * @memberof ModuleLibraryTasks
   */
  function assembleModuleLibrary() {

    var assemble = require('../lib/fabricator-assemble-custom');
    var options = config.fabricator.options;
    options.onError = function (err) {
      $.util.log(err.toString());
    };

    var crypto = require('crypto');
    var hash = (crypto.createHash('md5').update(Date.now().toString()).digest('hex')).substring(0, 10);

    /**
     * Get a hash to be used with static resource url's for cache busting
     * @returns {string|*}
     */
    var assetHash = function () {
      return hash;
    };

    /**
     * Get the dictionary for the lang set in the page
     * @param key
     * @returns {*}
     */
    var dict = function (key) {
      if (!this.lang) {
        return '{nd}';
      }

      var result = this.dictionary[this.lang];
      var keys = key.split('.');

      for (var index = 0; index < keys.length; index++) {
        if (result) {
          result = result[keys[index]];
        }
      }

      // return result;
      return new Handlebars.SafeString(result);
    };

    // set helpers
    options.helpers = {
      assetHash: assetHash,
      markdown: require('helper-markdown'),
      dict: dict
    };

    assemble(options);

  }


  /**
   * Compile the SASS fabricator styles and copy the resulting f.css file to the '/module-library/assets/fabricator/styles'
   * @method styles
   * @memberof ModuleLibraryTasks
   * @returns {*}
   */
  function styles() {

    // compile LESS for docs
    var scssStream = gulp.src(config.paths.scss.docs)
      .pipe($.plumber(util.plumberErrorHandler))
      // .pipe($.sourcemaps.init())
      .pipe($.scss())
      .pipe($.autoprefixer(config.autoprefixer));
    // .pipe($.csscomb('.csscomb.json'));
    // .pipe($.sourcemaps.write('./', { sourceRoot: '/src/assets/less/' }))
    // .pipe(gulp.dest('./dist/assets/css'));


    var sassStream = gulp.src(config.fabricator.styles.fabricator)
      .pipe($.plumber(util.plumberErrorHandler))
      // .pipe($.sourcemaps.init())
      .pipe($.sass().on('error', $.sass.logError))
      .pipe($.autoprefixer(config.autoprefixer));
    // .pipe($.rename('f.css'))
    // .pipe($.sourcemaps.write())
    // .pipe(gulp.dest(config.fabricator.options.dest + '/assets/fabricator/styles'));

    var mergedStream = merge(scssStream, sassStream)
      // .pipe($.sourcemaps.init())
      .pipe($.concat('docs.min.css'))
      // .pipe($.sourcemaps.write('./', { sourceRoot: '/src/assets/less-docs/' }))
      .pipe($.cssnano())
      .pipe(gulp.dest('./dist/assets/css'));

    return mergedStream;
  }


  // /**
  //  * deploy the modules in the 'deploy' folder, along with the required module stylesheets and javascripts.
  //  */
  // function deployModules() {
  //   // delete olf folder
  //   var del = require('del');
  //   del.sync('./dist/modules');
  //
  //   /**
  //    * get the module name without suffix to be used as folder name
  //    */
  //   function getFolderName(fileName) {
  //     var folderName = '';
  //     var parts = fileName.split('-');
  //     for (var index = 0; index < parts.length; index++) {
  //       if (index === 0) {
  //         folderName = folderName + parts[index] + '-';
  //         continue;
  //       }
  //
  //       if (!_.isNaN(_.parseInt(parts[index], 10))) {
  //         break;
  //       }
  //
  //       folderName = folderName + parts[index] + '-';
  //     }
  //
  //     if (folderName.substr(folderName.length - 1, 1) === '-') {
  //       folderName = folderName.substring(0, folderName.length - 1);
  //     }
  //
  //     return folderName;
  //   }
  //
  //   // copy modules html files
  //   return gulp.src('src/materials/components/**/*.html')
  //     .pipe($.plumber(util.plumberErrorHandler))
  //     .pipe(gulp.dest('./dist/modules'));
  //
  //
  //   // // copy modules js files
  //   // gulp.src('dist/assets/js/modules/**/*.js')
  //   //   .pipe($.plumber(util.plumberErrorHandler))
  //   //   .pipe(gulp.dest(function (file) {
  //   //
  //   //     var fileName = file.path.substring(file.base.length);
  //   //     var minIndex = fileName.indexOf('.min');
  //   //     if (minIndex > -1) {
  //   //       fileName = fileName.substring(0, minIndex) + '.js';
  //   //     }
  //   //
  //   //     return './deploy/modules/' + getFolderName(fileName.substring(0, fileName.lastIndexOf('.')));
  //   //   }));
  // }


  /**
   * Generate Arabic pages in the folder 'src/views/pages-in-arabic'
   * Warning: previous files can be overwritten
   */
  function generateArabicPages() {
    var src = 'src/views/pages-in-english/*.html';
    var dest = 'src/views/pages-in-arabic/';

    // gulp.src(src)
    //   .pipe($.plumber(util.plumberErrorHandler))
    //   .pipe($.replace('lang: en', 'lang: ar'))
    //   .pipe($.replace('direction: ltr', 'direction: rtl'))
    //   .pipe($.replace(/(language-switch-to:).*/, 'language-switch-to: '))
    //   .pipe(gulp.dest(dest));

    function replaceDynamicVariables(file) {
      var name = file.path.substr(file.path.lastIndexOf('\\') + 1);

      file.contents = new Buffer(String(file.contents)
        .replace('lang: en', 'lang: ar')
        .replace('direction: ltr', 'direction: rtl')
        .replace(/(language-switch-to:).*/, 'language-switch-to: ' + name)
      );
    }

    return gulp.src(src)
      .pipe($.tap(replaceDynamicVariables))
      .pipe($.rename(function (path) {
        var firstDashIndex = path.basename.indexOf('-');
        var fileNameAr = path.basename.substr(0, firstDashIndex + 1) + 'ar-' + path.basename.substr(firstDashIndex + 1);
        path.basename = fileNameAr;
        return path;
      }))
      .pipe(gulp.dest(dest));
    // .pipe(gulp.dest(function (file) {
    //   var fileName = file.path.substring(file.base.length);
    //   var fileNameAr = fileName.substr(0,3) + 'ar-' + fileName.substr(3);
    //   return dest + fileNameAr;
    // }));


    // var files = globby.sync(src, { nodir: true });
    //
    // // create output directory if it doesn't already exist
    // mkdirp.sync(dest);
    //
    // files.forEach(function (file) {
    //
    // });

  }


  return {
    assemble: assembleModuleLibrary,
    styles: styles,
    // deployModules: deployModules,
    generateArabicPages: generateArabicPages
  };

}

module.exports = ModuleLibraryTasks;
