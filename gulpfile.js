// ****************************************
// Gulpfile Documentation
// ****************************************

'use strict';

// include plug-ins
var fs = require('fs');
var path = require('path');
var request = require('request');
var sequence = require('run-sequence');
var rimraf = require('rimraf');
var browser = require('browser-sync').create();
var _ = require('lodash');

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
$.scss = require('gulp-sass');

var config = require('./gulp.config.js')();
var util = require('./tasks/util')($);
var exec = require('child_process').exec;

// var dependo = require('gulp-dependo');
var scssTasks = require('./tasks/scss-tasks')(gulp, $, config, browser);
// var lintTasks = require('./tasks/lint-tasks')(gulp, $, config);
var copyTasks = require('./tasks/copy-tasks')(gulp, $, config);
var moduleLibraryTasks = require('./tasks/module-library-tasks')(gulp, $, config);
var deployModulesTasks = require('./tasks/deploy-modules-tasks')(gulp, $, config);
// var emailsTasks = require('./tasks/emails-tasks')(gulp, $, config);


/**
 * Delete the "dist" folder
 * @method clean
 */
gulp.task('clean', function (done) {
  rimraf('./dist', done);
});


/**
 * Restore all bower packag
 * @method bower-resto
 */
gulp.task('bower-restore', function () {
  return $.bower()
    .pipe($.plumber(util.plumberErrorHandler));
});


// -------------------------------------
// Task: scss
// preprocess Bootstrap scss files with
// sourcemaps
// -------------------------------------
gulp.task('scss', function (cb) {
  return scssTasks.scss(cb);
});

gulp.task('scssModules', function () {
  return scssTasks.scssModules();
});


// ************************************************
// *                LINTERS
// ************************************************

// -------------------------------------
// Task: eslint
// -------------------------------------
// gulp.task('eslint', function () {
//   return lintTasks.eslint();
// });


// ************************************************
// *              COPY TASKS
// ************************************************

// -------------------------------------
// Task: copy:fonts
// Copy files out of the assets folder
// -------------------------------------
gulp.task('copy:fonts', function () {
  return copyTasks.copyFonts();
});

// -------------------------------------
// Task: copy:js
// Copy files out of the app folder
// -------------------------------------
gulp.task('copy:js', function () {
  return copyTasks.copyJs();
});

// -------------------------------------
// Task: copy:css
// Copy files out of the app folder
// -------------------------------------
gulp.task('copy:css', function () {
  return copyTasks.copyCss();
});

// -------------------------------------
// Task: copy:others
// Copy files out of the app folder
// -------------------------------------
gulp.task('copy:others', function () {
  return copyTasks.copyOthers();
});

// -------------------------------------
// Task: copy:mockdata
// Copy files out of the app folder
// -------------------------------------
gulp.task('copy:mockdata', function () {
  return copyTasks.copyMockData();
});


// -------------------------------------
// Task: copy:favicon
// Copy files out of the app folder
// -------------------------------------
gulp.task('copy:favicon', function () {
  return copyTasks.copyFavicon();
});

// -------------------------------------
// Task: copy:images
// Copy images
// -------------------------------------
gulp.task('copy:images', function () {
  return copyTasks.copyImages();
});

// -------------------------------------
// Task: copy:images
// Copy images
// -------------------------------------
// gulp.task('copy:kml', function () {
//   return copyTasks.copyKML();
// });

// -------------------------------------
// Task: images
// Optimize images
// -------------------------------------
gulp.task('images', function () {
  return copyTasks.images();
});


// ************************************************
// *        MODULE LIBRARY Documentation
// ************************************************

// * Build the Module Library documentation running Fabricator
gulp.task('module-library:docs', function (done) {
  moduleLibraryTasks.assemble();

  done();
});


// * Compile the Module Library styles (Fabricator) into CSS
gulp.task('module-library:styles', function () {
  return moduleLibraryTasks.styles();
});

// * Generate arabic pages from english ones
gulp.task('module-library:generate-arabic-pages', function () {
  return moduleLibraryTasks.generateArabicPages();
});


// * Copy the Module Library scripts (Fabricator) to the dist/module-library folder
gulp.task('deploy-modules', function (done) {
  deployModulesTasks.deployModules();

  return done;
});


// *************************************************
// *              WEB SERVER
// ************************************************
gulp.task('server:node', function (cb) {
  exec('node ./server/index.js', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});


// Start a server with LiveReload to preview the site in
gulp.task('server', function () {
  browser.init({
    server: 'dist',
    injectFileTypes: ['scss'],
    port: config.port,
    online: true
  });
});

gulp.task('server:offline', function () {
  browser.init({
    server: 'dist',
    port: config.port,
    online: false,
    codeSync: false,
    injectChanges: false,
    minify: false
  });
});


// ************************************************
// *              Emails tasks
// ************************************************
// gulp.task('emails:clean', function (done) {
//   return emailsTasks.clean(done);
// });

// gulp.task('emails:zip', function (done) {
//   return emailsTasks.zip();
// });

// gulp.task('emails:inline', function (done) {
//   return emailsTasks.inline();
// });

// gulp.task('emails:scss', function (done) {
//   return emailsTasks.scss();
// });

// gulp.task('emails:images', function (done) {
//   return emailsTasks.images();
// });

// gulp.task('emails:compile', function (done) {
//   return emailsTasks.compile();
// });


/**
 * Email Build
 * clean, pages, sass, images, inline
 */
// gulp.task('emails:build', function (done) {
//   sequence(
//     'emails:clean',
//     'emails:scss',
//     'emails:images',
//     'emails:compile',
//     'emails:inline',
//     done);
// });

/**
 * Watch for file changes on emails files
 */
// gulp.task('emails:watch', function () {
//   gulp.watch(['emails/src/pages/*.html', 'emails/src/layouts/*.html', 'emails/src/partials/**/*.html'], ['emails:compile'], browser.reload);
//   gulp.watch(['emails/src/assets/scss/**/*.scss'], 'emails:scss', browser.reload);
//   // gulp.watch('emails/src/assets/img/**/*', 'emails:images', browser.reload);
// });


// ************************************************
// *              GULP DEFAULT TASKs
// ************************************************

// -------------------------------------
// Task: lint
// Run all linters
// -------------------------------------
// });
// gulp.task('lint', ['eslint'], function () {});

// -------------------------------------
// Task: Build the Module Library
// -------------------------------------

gulp.task('module-library', [
  'module-library:docs',
  'module-library:styles'
], function () {});


// -------------------------------------
// Task: build
// Build the "dist" folder by running all of the above tasks
// -------------------------------------
gulp.task('build', function (done) {
  sequence(
    ['clean', 'bower-restore'],
    ['scss'],
    ['copy:css', 'copy:js', 'copy:fonts', 'copy:others', 'copy:favicon', 'copy:images', 'copy:mockdata'],
    // 'lint',
    ['module-library'],
    // ['emails:build'],
    done);
});


// -------------------------------------
// Task: watch
// -------------------------------------
gulp.task('watch', function () {
  // gulp.watch(['src/assets/js/*.js', 'src/assets/js/modules/*.js', 'src/assets/js/forms/*.js' ], ['lint', 'copy:js', browser.reload]);
  gulp.watch(['src/assets/js/*.js', 'src/assets/js/modules/*.js', 'src/assets/js/forms/*.js'], ['copy:js', browser.reload]);
  gulp.watch(['src/assets/scss/*.scss'], ['scss']);
  gulp.watch(['src/assets/scss/mixins/*.scss'], ['scss']);
  gulp.watch(['src/assets/scss/modules/*.scss'], ['scss']);
  // gulp.watch(['src/assets/scss/pickadate/*.scss'], ['scss']);
  gulp.watch(['src/assets/img/**/*'], ['copy:images', browser.reload]);
  gulp.watch(['src/assets/mock-data/*.json'], ['copy:mockdata', browser.reload]);

  gulp.watch([
    'src/materials/**/*.{html,md}',
    'src/data/**/*.json',
    'src/docs/*.md',
    'src/views/**/*.html',
    'src/layouts/**/*.html'
  ], ['module-library:docs', browser.reload]);

  gulp.watch(['src/assets/scss-docs/**/*.scss'], ['module-library:styles', browser.reload]);

});


// -------------------------------------
// Task: default
// Build the site, run the server, and watch for file changes
// -------------------------------------
gulp.task('default', function (done) {
  sequence('build', 'server', 'watch', done);
});
