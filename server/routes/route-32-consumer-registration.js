var routes = function(router) {
  router.route('/consumer-registration-prepaid-step1')
    .post(function (req, res) {
      res.status(200).json({
        success: true,
        message: '',
        data: {
          url: '17-consumer-registration-prepaid-step2.html'
        }
      });

      // res.status(500).json({
      //   success: false,
      //   message: 'Boooooh!',
      //   data: null
      // });
    });

};

module.exports = routes;

/**
 * API for Module 32 Registration
 * -------------------------------------------------------------------------------
 */
$.mockjax({
  url: '*api/consumer-registration',
  data: { consumerNumber: '1', accountNumber: '111' },
  dataType: 'json',
  type: 'POST',
  responseText: { success: true, message: '200' },
  status: 200,
  responseTime: [800, 1800] // randomize latency to range 800-1800ms
  // isTimeout: true
});

$.mockjax({
  url: '*api/consumer-registration',
  data: '*',
  // data: { consumerNumber: '2', accountNumber: '222' },
  dataType: 'json',
  type: 'POST',
  responseText: { success: false, message: '403' },
  status: 403,
  responseTime: [800, 1800] // randomize latency to range 800-1800ms
  // isTimeout: true
});
