/**
 * Simple API Server
 */


// TODO: set static asset folder to dist!

// BASE SETUP
// =============================================================================
var logger = require('morgan');

// call the packages we need
var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');

// configure logger
app.use(logger('dev'));

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Static Folder
app.use(express.static('../dist'));

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
// router.get('/api', function (req, res) {
//   res.json({ message: 'hooray! welcome to our api!' });
// });


// REGISTER OUR ROUTES -------------------------------
router.route('/consumer-registration-prepaid-step1')
  .post(function (req, res) {
    res.status(200).json({
      success: true,
      message: '',
      data: {
        url: '17-consumer-registration-prepaid-step2.html'
      }
    });

    // res.status(500).json({
    //   success: false,
    //   message: 'Boooooh!',
    //   data: null
    // });
  });

// api/reset-password-step3
router.route('/reset-password-step3')
  .post(function (req, res) {
    // res.status(200).json({
    //   success: true,
    //   message: '',
    //   data: {
    //     url: '17-consumer-registration-prepaid-step2.html'
    //   }
    // });
    res.status(500).json({
      success: false,
      message: 'Boooooh!',
      data: null
    });
  });

// api/consumer-registration
router.route('/consumer-registration')
  .post(function (req, res) {
    // res.status(200).json({
    //   success: true,
    //   message: '',
    //   data: {
    //     url: '17-consumer-registration-prepaid-step2.html'
    //   }
    // });
    res.status(403).json({
      success: false,
      message: '4 0 3 !',
      data: null
    });
  });

router.route('/consumer-registration-offline')
  .post(function (req, res) {
    // res.status(200).json({
    //   success: true,
    //   message: '',
    //   data: {
    //     url: '17-consumer-registration-prepaid-step2.html'
    //   }
    // });
    res.status(403).json({
      success: false,
      message: '4 0 3 !',
      data: null
    });
  });


router.route('/force-password-change-step1')
  .post(function (req, res) {
    res.status(200).json({
      success: true,
      message: '',
      data: {
        url: '17-business-force-change-password-step2.html'
      }
    });
    // res.status(403).json({
    //   success: false,
    //   message: '4 0 3 !',
    //   data: null
    // });
  });
router.route('/force-password-change-step2')
  .post(function (req, res) {
    res.status(200).json({
      success: true,
      message: '',
      data: {
        url: '100-b2b-home.html'
      }
    });
    // res.status(403).json({
    //   success: false,
    //   message: '4 0 3 !',
    //   data: null
    // });
  });

router.route('/resend-token')
  .post(function (req, res) {
    res.status(200).json({
      success: true,
      message: '',
      data: null
    });
  });

router.route('/check-availability')
// .post(function (req, res) {
//   res.status(200).json({
//     success: true,
//     data: {
//       availability: true,
//       url: '06-b2c-configuration-device-to-plan-my-number.html?eid=2&nt=102'
//     }
//   });
// });
  .post(function (req, res) {
    res.status(200).json({
      success: true,
      data: {
        availability: false
      }
    });
  });


router.route('/check-availability-account')
  .post(function (req, res) {
    res.status(200).json({
      success: true,
      data: {
        availability: true,
        lat: 25.218569,
        lng: 55.283602,
        description: 'Business description of the selected account'
      }
    });
  });


// middleware to use for all requests
router.use(function (req, res, next) {
  // do logging
  console.log('METHOD: %s URL: %s PATH: %s', req.method, req.url, req.path);
  next(); // make sure we go to the next routes and don't stop here
});


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);


// START THE SERVER
// =============================================================================
var port = process.env.PORT || 4321;

app.listen(port, function () {
  console.log('REST API Server started and listening on http://localhost:' + port);
});
