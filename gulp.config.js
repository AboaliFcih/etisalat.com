//
// GULP file configuration
//
module.exports = function () {
  var config = {
    // Port to use for the development server.
    port: 4200,

    paths: {
      // settings paths
      settings: {
        eslint: './.eslintrc',
        csslint: './.csslintrc',
        jsdoc: './.jsdoc'
      },
      css: [
        './src/assets/css/**/*'
      ],
      images: [
        './src/assets/img/**/*'
      ],
      favicon: [
        './src/assets/favicon.ico'
      ],
      mockdata: [
        './src/assets/mock-data/**/*.json'
      ],
      fonts: [
        './src/assets/fonts/**/*'
      ],
      scssIncludes: [
        'lib/bootstrap/scss'
      ],
      scss: {
        app: ['./src/assets/scss/app.scss'],
        docs: ['./src/assets/scss-docs/docs.scss']
      },
      javascript: {
        core: [

        ],
        js: [
          'src/assets/js/**/*.js',

          '!src/assets/js/modules/*.js',
          '!src/assets/js/app/**/*.js',
          '!src/assets/js/r.js',

          'bower_components/requirejs-plugins/src/async.js',

          'src/assets/js/docs.js',
          'src/assets/js/lodash.js',

          'src/assets/js/jquery.js',

          'node_modules/owl.carousel/dist/owl.carousel.min.js',

          'bower_components/swiper/dist/js/swiper.min.js'
        ],
        datepicker: [

        ],
        timepicker: [

        ],
        validation: [],
        modules: [
          'src/assets/js/modules/*.js'
        ]
      },
      others: [
        './src/robots.txt',
        './src/web.config',
        './src/azure-login.aspx'
      ],
      kml: [
        './src/assets/kml/*.kml'
      ]
    },

    // autoprefixer config
    autoprefixer: [
      'Android 2.3',
      'Android >= 4',
      'Chrome >= 30',
      'Firefox >= 30',
      'Explorer >= 10',
      'iOS >= 6',
      'Opera >= 12',
      'Safari >= 6'
    ],

    fabricator: {
      styles: {
        fabricator: 'src/assets/scss-docs/fabricator.scss'
      },
      options: {
        layouts: ['src/layouts/*'],
        layoutIncludes: ['src/layouts/includes/*'],
        views: ['src/views/**/*', '!src/views/+(layouts)/**'],
        materials: ['src/materials/**/*.html'],
        emails: ['emails/src/pages/**/*.html'],
        data: ['src/data/**/*.{json,yml}'],
        docs: ['src/docs/**/*.md'],
        dest: 'dist/',
        logErrors: true,
        beautify: false,
        debug: false
      }
    }

  };

  return config;
};