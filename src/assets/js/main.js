﻿requirejs.config({
  paths: {
    waitSeconds: 40,
    jquery: ['https://code.jquery.com/jquery-3.2.1.slim.min'],
    popper: ['https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min'],
    bootstrap: ['https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min'],
    owlcarousel: ['/node_modules/owl.carousel/dist/owl.carousel.min'],
    initBootstrap: ['...wotever...']
  },
  shim: {
    bootstrap: ['jquery'],
    'app.core.min': {
      deps: ['jquery']
    },
    owlcarousel: {
      deps: ['jquery']
    }
  }
});

require(['jquery', 'popper', 'app.core.min'], function ($, popper, app) {
  window.Popper = popper;
  require(['bootstrap']);
  app();
});
