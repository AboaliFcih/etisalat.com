 /*eslint-disable */
 /**
  * v###version###
  */
 (function (define, window, document) {
   define(['jquery', 'app.core.min', 'swiper.min'], function ($, app, swiper) {
     app();
     swiper();
     return function () {
       var swiper = new Swiper('.projects-boxes', {
         slidesPerView: 3,
         spaceBetween: 16,
         breakpoints: {
           768: {
             slidesPerView: 'auto',
             spaceBetween: 20,
           }
         }
       });
     };
   });

 }(define, window, document));