/**
* v###version###
*/
(function (define, window) {
  define(['jquery', 'app.core.min', 'select2.min', 'jquery.validate.min', 'app.forms.defaults.min'], function ($, app, select2, val, valdefault) {
    'use strict';

    return function () {
      // [required] init the validation defaults
      valdefault();

      $(document).ready(function () {

        $('select[name=typeRequest]', $form).on('change', function (e) {
          var $selectedOption = $('option:selected', $(this));
          var selectedMethod = $(this).val();
          if (selectedMethod === 'Block/Unblock URL') {
            $('.block-url').removeClass('hidden');
          } else {
            $('.block-url').addClass('hidden');
          }
        });

        /**
        * FORM VALIDATION
        */

        // cache the submit button
        var $form = $('.contact-form');
        var submitButtonContainer = $('.form-submit');
        var loadingContainer = $('.form-submitting');
        var resultMessageContainer = $('.form-submitted');

        // init the datepicker
        var currentDate = new Date();

        $('.datepicker.date').pickadate({
          selectYears: true,
          selectMonths: true,
          format: 'dd/mm/yyyy',
          formatSubmit: 'dd/mm/yyyy',
          min: false,
          max: false,
          onSet: function (thingSet) {
            this.$node.parent('.floating-label-input').find('label').addClass('floating-label');
          }
        });

        $('.datepicker:not(.birthday)').pickadate({
          disable: [6],
          selectYears: true,
          selectMonths: true,
          format: 'dd/mm/yyyy',
          formatSubmit: 'dd/mm/yyyy',
          min: new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()),
          onSet: function (thingSet) {
            this.$node.parent('.floating-label-input').find('label').addClass('floating-label');
          }
        });


        $('.datepicker.birthday').pickadate({
          selectYears: 79,
          selectMonths: true,
          format: 'dd/mm/yyyy',
          formatSubmit: 'dd/mm/yyyy',
          max: new Date(currentDate.getFullYear() - 21, currentDate.getMonth(), currentDate.getDate()),
          onSet: function (thingSet) {
            this.$node.parent('.floating-label-input').find('label').addClass('floating-label');
          }
        });


        $('.timepicker.time').pickatime({
          format: 'h:i A',
          min: [9, 0],
          max: [18, 0],
          onSet: function (thingSet) {
            this.$node.parent('.floating-label-input').find('label').addClass('floating-label');
          }
        });


        $form.validate({
          submitHandler: function (form) {

            // get the form data before disabling fields (otherwise they don't get value)
            var formData = $form.serialize();

            // hide the submit button, disable the form, and show the loadings
            submitButtonContainer.addClass('hidden');
            $('input, select', $form).prop('disabled', true); // disable all input and select of the form
            loadingContainer.removeClass('hidden');

            $.ajax({
              type: 'POST',
              url: $(form).attr('action'),
              data: formData,
              dataType: 'json',
              encode: true
            })
            .done(submitSuccessResponse)
            .fail(submitErrorResponse);

            // return false to prevent normal browser submit and page navigation
            return false;
          }
        });


        /**
        * Callback for successfull ajax submit
        */
        var submitSuccessResponse = function (json, statusText, xhr) {
          if (json.success && json.data && json.data.url) {
            window.location.href = json.data.url;
            return true;
          }

          // in case of failure show a message
          var errorText = json ? json.message : statusText;
          showErrorMessage(errorText);

          loadingContainer.addClass('hidden');
          // hide whole form
          $form.removeClass('hidden');
          return true;
        };

        /**
        * Callback for ajax submit with error
        * @param jqXHR
        * @param textStatus
        * @param error
        */
        var submitErrorResponse = function (jqXHR, textStatus, error) {
          var errorText = jqXHR.responseJSON && jqXHR.responseJSON.message || error;
          showErrorMessage(errorText);
          loadingContainer.addClass('hidden');
        };

        /**
        * Common function to display error messages
        * @param text
        */
        function showErrorMessage (text) {
          $('h3', resultMessageContainer).text(text);
          resultMessageContainer.addClass('error');
          resultMessageContainer.removeClass('hidden');

          // show again the submit button, enable the form, hide the loadings
          submitButtonContainer.removeClass('hidden');
          $('input, select', $form).prop('disabled', false); // enable all input and select of the form
        }

        // tooltip
        var $tooltipElements = $('[data-toggle="tooltip"]');
        if ($tooltipElements.length > 0) {
          $tooltipElements.tooltip();
        }

        // init visualCaptcha servlet
        visualCaptcha();

        // input file
        var $uploadButton = $('input[type=file]');
        var fileSelected = 0;
        var filesArray = [];
        $uploadButton.change(function () {
          var _this = $(this);
          if (_this.val() === '') {
            filesArray = [];
            fileSelected = 0;
            _this.prev('.inputfile-label').removeClass('uploaded').text('Not uploaded yet');
            return false;
          }
          for (var i = 0; i < _this[0].files.length; ++i) {
            var name = _this[0].files.item(i).name;
            filesArray.push(name);
            if (filesArray.length <= 3) {
              fileSelected++;
            } else {
              filesArray.pop();
              alert(_this.data('alert'));
            }
          }
          _this.prev('.inputfile-label').addClass('uploaded').text(filesArray.join(' | '));

          return true;
        });

      });
    };
  });
}(define, window));
