# Project Setup & Build

## Tools & Runtime:
### Windows Setup:
	• Install Microsoft Visual C++ 2010 SP1 Redistributable Package (x64): https://www.microsoft.com/en-us/download/details.aspx?id=13523 
	• Install NodeJS LTS version (at the moment of writing the latest is v4.4.6): https://nodejs.org/dist/v4.4.6/node-v4.4.6-x64.msi 
	• Install GIT for Windows: https://git-for-windows.github.io/ 

### Max OSX Setup:
	• Install Xcode (via App Store)
	• Install NodeJS: https://nodejs.org/dist/v4.4.6/node-v4.4.6.pkg

## Getting the Source Code
The source code is stored in a GIT repository; if you don’t know well GIT please take a look at this simple guide: [http://rogerdudler.github.io/git-guide/]()

To do a checkout from the repository, open a terminal/command prompt, change directory to a proper folder (i.e. c:\projects), 
and run the following command (eventually replace the repository url if it's changed):

`git clone https://tfs13.dcs.avanade.com/tfs/ISCW/_git/Etisalat etisalat`

## One time installation
Once all is downloaded, open a terminal/command prompt on the “etisalat” folder, and run the following commands in the exact order:

	1. Go to your projects folder (or create one), i.e. c:\projects and open a command prompt in there
	2. Run this command to globally install Gulp, Bower and RimRaf: `npm install -g gulp bower rimraf`
	3. Run the following command: `git clone https://tfs13.dcs.avanade.com/tfs/ISCW/_git/Etisalat etisalat`
	4. Run: `cd Etisalat`
	5. (this can take several minutes to complete)Run: `npm install`
	6. If you are working behind a proxy (Avanade/Accenture network), run this command:`git config --global url."http://".insteadOf git://`
	7. Run: `bower install`


## Project Build
This project use several GULP (see: [http://gulpjs.com/]() and [https://www.smashingmagazine.com/2014/06/building-with-gulp/]() )  
for various tasks; now in the terminal/command prompt at the “Etisalat” folder run this command:
`gulp default`
 
This will run the default tasks that will launch the other sub-tasks:
- build: run in sequence a group of tasks to compile the Less files, assemble the html, copy images, js, css to the “dist” folder
- server: run the NodeJs web server on the “dist” folder; by default is configured to the url [http://localhost:4321]()
- watch: activate the file watchers on some src folders, so that as soon as a change is made on these files, automagically they are assembled and copied in the “dist” folder
 
Alternatively, you can run the main tasks one by one:
 
`gulp build`
 
`gulp watch`
 
`gulp server`
 
Please note that the tasks default, watch, and server runs until explicit stopped. 


> All the HTML is developed using a custom version of Fabricator [http://fbrctr.github.io/]() and at its core make use of the HandlebarsJS template engine [http://handlebarsjs.com/]()


## Project Folder Organization
A rapid note on the project folders:
 
#### src
main source code folder
 
#### src/assets
folder with the stylesheets, Less files, images, fonts, etc… .
 
#### src/materials/components
All modules thml
 
#### src/materials/styleguide
All html partials that compose the live styleguide
 
#### src/layouts
Contains the main master page, “default.html”, and some partials used in the documentation pages
 
#### src/views
Contains the full pages definitions built as a bundle of components. The pages are grouped in different folders for both languages; they reference all the required moduels, using the handlebars partial syntax.

#### emails/src
Root tree for the Email Templates built with the framework Foundation for Emails 2 ([http://foundation.zurb.com/emails.html]())
In the project has been integrated the SASS version, as reference see: ([http://foundation.zurb.com/emails/docs/sass-guide.html]()).
It works in a very similar way to build the email templates by using handlebars partials.

There are dedicated Gulp tasks, and the main is `emails:build`; a watcher is available with the task `emails:watch`.
The `emails:build` task has also been added to the usual `build` task we already use.

The compiled templates are deployed in the `dist/emails` folder. From the docs site, in the left sidebar there’s a dedicated menu that access to the page http://localhost:4321/emails.html
In this page you can browse each template, and try also the responsive view ;)

As mentioned in the Foundation site, the best tool to test such emails with several email client, si to use the the SaaS tool ([https://litmus.com](Litmus))
You can create a free account that last 3 months, which allow you to test emails only with 3 clients: Outlook 2013, Gmail (chrome), iPhone6s.

As integration notes, please consider what follow:
*	All templates refer an external css: the url must be changed with the real one on the CMS.
*	All images are external: they must be deployed on the production server, and their src attribute must be changed accordingly on the CMS.
* for Content Editors, notes that the images to be used in the templates must not have sizes bigger than the defaults provided by Design; otherwise the layout may broke (this is a side effect for the bad support of email client applications).
