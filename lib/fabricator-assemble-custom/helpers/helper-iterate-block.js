var Handlebars = require('handlebars');


/**
 * Block iteration
 * @description Repeat a block a given amount of times.
 * @example
 * {{#iterate-block 20}}
 *   <li>List Item {{@index}}</li>
 * {{/iterate-block}}
 */
module.exports = function (n, block) {
	var accum = '', data;
	for (var i = 0; i < n.length; ++i) {
		if (block.data) {
			data = Handlebars.createFrame(block.data || {});
			data.index = n[i];

		}
		accum += block.fn(i, {data: data});		
	}
	return accum;
};
